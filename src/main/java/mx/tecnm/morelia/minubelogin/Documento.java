
package mx.tecnm.morelia.minubelogin;

import java.io.ObjectOutputStream;
import java.net.Socket;
import mx.tecnm.morelia.minubelogin.newpackage.Paquete;

public class Documento extends javax.swing.JFrame {
    String contenido;
    String nombreArchivo;
    String usuario;
    private String rutaArchivo = "C:/Users/Andrea/Documents/Cloud/"+usuario+"/"+nombreArchivo;
    
    public Documento() {
    }
    
    public Documento(String contenido, String nombreArchivo,String usuario){
        initComponents();
        this.contenido = contenido;
        this.nombreArchivo = nombreArchivo;
        this.usuario = usuario;
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        textAreaMostrar = new javax.swing.JTextArea();
        labelNombreArchivo = new javax.swing.JLabel();
        btnGuardarCambios = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        textAreaMostrar.setColumns(20);
        textAreaMostrar.setRows(5);
        jScrollPane1.setViewportView(textAreaMostrar);

        labelNombreArchivo.setFont(new java.awt.Font("Microsoft JhengHei", 3, 18)); // NOI18N
        labelNombreArchivo.setText("Documentos: ");

        btnGuardarCambios.setText("Guardar Cambios");
        btnGuardarCambios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarCambiosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelNombreArchivo, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGuardarCambios))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelNombreArchivo, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 409, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardarCambios, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        labelNombreArchivo.setText(nombreArchivo);
        textAreaMostrar.setText(contenido);
    }//GEN-LAST:event_formWindowOpened

    private void btnGuardarCambiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarCambiosActionPerformed
        contenido = textAreaMostrar.getText();
        System.out.println("Contenido\n" + contenido);
        try {
            byte[] documento = contenido.getBytes();
            enviarArchivo(documento);
        } catch (Exception e) {
            System.out.println("Error al editar documento");
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnGuardarCambiosActionPerformed
    
    private void enviarArchivo(byte[] documento){
        System.out.println(nombreArchivo);
        System.out.println(documento);
        Socket socket = null;
        ObjectOutputStream out = null;
        Paquete paquete = new Paquete(nombreArchivo, documento, Paquete.SUBIR);
        try {
            socket = new Socket("localhost", 5678);
            
            out = new ObjectOutputStream(socket.getOutputStream());
            
            out.writeObject(paquete);
            
            out.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Documento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Documento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Documento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Documento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Documento().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardarCambios;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelNombreArchivo;
    private javax.swing.JTextArea textAreaMostrar;
    // End of variables declaration//GEN-END:variables
}
