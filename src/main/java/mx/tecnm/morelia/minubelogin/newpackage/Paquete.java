
package mx.tecnm.morelia.minubelogin.newpackage;

import java.io.Serializable;
import java.nio.file.Files;

public class Paquete implements Serializable{
    
    public static final int LISTAR = 0;
    public static final int ELIMINAR = 1;
    public static final int EDITAR = 2;
    public static final int SUBIR = 3;
    public static final int DESCARGAR = 4;
    public static final int VER = 5;
    
    String nombreArchivo;
    String contenido;
    Files archivos[];
    byte[] archivo;
    int operacion;

    public Paquete() {
    }

    public Paquete(String nombreArchivo, String contenido, Files[] archivos, byte[] archivo, int operacion) {
        this.nombreArchivo = nombreArchivo;
        this.contenido = contenido;
        this.archivos = archivos;
        this.archivo = archivo;
        this.operacion = operacion;
    }

    public Paquete(String nombreArchivo, byte[] archivo, int operacion) {
        this.nombreArchivo = nombreArchivo;
        this.archivo = archivo;
        this.operacion = operacion;
    }
    
    

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Files[] getArchivos() {
        return archivos;
    }

    public void setArchivos(Files[] archivos) {
        this.archivos = archivos;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public int getOperacion() {
        return operacion;
    }

    public void setOperacion(int operacion) {
        this.operacion = operacion;
    }

    @Override
    public String toString() {
        String cadena = "\nNombre Archivo: " + nombreArchivo;
        cadena += "\n Contenido: " + contenido;
        cadena += "\n Total Archivos: " + (archivos != null ? archivos.length : 0);
        cadena += "\n Tamanio Archivo: " + (archivo != null ? archivo.length : 0);
        cadena += "\n Operacion: " + operacion;
        return cadena;
    }
    
    

    
}
