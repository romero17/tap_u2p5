
package mx.tecnm.morelia.minubelogin.newpackage;

import java.io.Serializable;


public class Usuario implements Serializable{
    String user;
    String pass;
    String name;

    public Usuario(){}
    
    public Usuario(String user, String pass, String name) {
        this.user = user;
        this.pass = pass;
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    
}
